# Script para criar mapa com os pontos de Harpia

# Bibliotecas
library(ggplot2)
library(sf)
library(rnaturalearth)
library(rnaturalearthdata)

world <- ne_countries(scale = "medium", returnclass = "sf")

# Lendo os dados das coordenadas limpas
coord <- read.csv("data/Tapirus.csv")

coord_ok <- coord %>%
  filter(.summary)

coord_flag <- coord %>%
  filter(!.summary)

table(coord$year)

# Criando o mapa ---------------------------------------------------------------
mapa1 <-
  # Cria janela de plot
  ggplot() +
  # Adiciona limite do mapa
  geom_sf(data = world) +
  # Adiciona limite para os eixos
  coord_sf(xlim = c(-140, 30), ylim = c(-70, 50), expand = FALSE) +
  # Adiciona os pontos das especies
  geom_point(data = coord, aes(x = decimalLongitude, y = decimalLatitude),
             size = 1, alpha = .5) +
  # Rotulos dos eixos
  xlab("") + ylab("") +
  theme_minimal()

mapa1
# Exportando o mapa
ggsave("figs/mapa_Tapirus1.png", height = 4, width = 6)

mapa2 <-
  # Cria janela de plot
  ggplot() +
  # Adiciona limite do mapa
  geom_sf(data = world) +
  # Adiciona limite para os eixos
  coord_sf(xlim = c(-140, 30), ylim = c(-70, 50), expand = FALSE) +
  # Adiciona os pontos das especies
  geom_point(data = coord_ok, aes(x = decimalLongitude, y = decimalLatitude),
             size = 1, alpha = .5) +
  geom_point(data = coord_flag, aes(x = decimalLongitude, y = decimalLatitude),
             size = 1, alpha = 2, color = "red") +
  # Rotulos dos eixos
  xlab("") + ylab("") +
  theme_minimal()

mapa2

# Exportando o mapa
ggsave("figs/mapa_Tapirus2.png", height = 4, width = 6)
